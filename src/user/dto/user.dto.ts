import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MaxLength, MinLength } from 'class-validator';

import { User } from '../entities/user.entity';
import { IsUnique } from 'src/etc/validator/exist-validator/unique-validator';
import { IsExist } from 'src/etc/validator/exist-validator/exist-validator';

export class UserDto {
  @ApiProperty()
  @IsExist ([User, 'id'])
  id: number;

  @ApiProperty({ required: true })
  @IsString()
  @MaxLength(200)
  nama_user: string;

  @ApiProperty({ required: true })
  @IsEmail()
  @IsUnique ([User, 'email'])
  email: string;

  @ApiProperty({ required: true })
  @IsString()
  @MinLength(6)
  @MaxLength(50)
  @IsUnique([User, 'username'])
  username: string;

  @ApiProperty({ required: true })
  @IsString()
  @MinLength(8)
  @MaxLength(32)
  password: string;
}