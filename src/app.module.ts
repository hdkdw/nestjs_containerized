import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { UserModule } from './user/user.module';
import { User } from './user/entities/user.entity';
import { ExistValidator } from './etc/validator/exist-validator/exist-validator';
import { AuthModule } from './auth/auth.module';
import { UniqueValidator } from './etc/validator/exist-validator/unique-validator';

@Module({
  imports: [
    ConfigModule.forRoot(),
    TypeOrmModule.forRoot({
      type : 'mysql',
      host : process.env.MYSQL_HOST,
      port : parseInt(process.env.MYSQL_PORT),
      username : process.env.MYSQL_USER,
      password : process.env.MYSQL_PASSWD,
      database : process.env.MYSQL_DB,
      entities : [
        User

      ],
      synchronize : true

    }),
    UserModule,
    AuthModule
  ],
  controllers: [AppController],
  providers: [AppService, ExistValidator, UniqueValidator],
})
export class AppModule {}
